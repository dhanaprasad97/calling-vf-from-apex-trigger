public without sharing class LP_CID_InspectionTriggerHandler extends TriggerHandler{
    public static Id insCIDRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('CID_Inspection').getRecordTypeId(); 
    public static Id insEventsRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Special_Events').getRecordTypeId(); 
    /* Added by Ankit S-15766 */
    public static Id inspectionStopWorkOrderId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Removal_of_Stop_Work_Order').getRecordTypeId(); 

    public override void afterUpdate(List<WorkOrder> newList){
        sendEmailForGenerationCertificate(newList);
    }

     /**
     * @author Dhana Prasad
     * @desc whenever to call a VF from trigger use future callout
     * 
     */
    @future(callout = true)
     public static void sendEmailForGenerationCertificate(List<WorkOrder> newList){
        List<WorkOrder> wolist= [SELECT Id, Application__c, Application__r.Contact__c,Application__r.Contact__r.Email,Application__r.Contact__r.LastName,Application__r.Project__c
                                FROM WorkOrder WHERE Id IN : newList];
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
        EmailTemplate template = [Select Id,Name,DeveloperName,HtmlValue,Subject,Body FROM EmailTemplate WHERE DeveloperName='LP_CID_Email_For_Issuance_Certification'];
        EmailTemplate templateForTemproryCert = [Select Id,Name,DeveloperName,HtmlValue,Subject,Body FROM EmailTemplate WHERE DeveloperName='LP_CID_Email_For_Issuance_Temprory_Certification'];
        for(WorkOrder wo : wolist){
                PageReference defaultPage = Page.LP_CID_PermanetCertOccupancy;
                defaultPage.getParameters().put('id',wo.Id);
                defaultPage.setRedirect(true);

                Blob pageData; 
                if(Test.isRunningTest()){	
                    pageData=Blob.valueOf('This is version data');
                }else{
                    pageData = defaultPage.getContentAsPdf(); 
                }
                String attchmentId  = createFileResponse('Certificate of Occupancy', pageData, new Set<Id>{wo.Id, wo.Application__c, wo.Application__r.Project__c });
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName('Certificate of Occupancy.pdf');
                efa.setBody(pageData);
                email.setToAddresses(new List<String>{wo.Application__r.Contact__c});
                email.setTemplateId(template.Id);
                email.setOrgWideEmailAddressId( orgWideEmailId );
                email.setTargetObjectId(wo.Application__r.Contact__c);
                email.setTreatTargetObjectAsRecipient(false);
                email.setWhatId(wo.Id);
 
                email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                // email.setTargetObjectId(wo.Id);
                email.setSaveAsActivity(true);
 
                // Sends the email
                    Messaging.SendEmailResult [] r = 
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
        
        }

     }
 /**
     * @author Dhana Prasad
     * @desc whenever the inspection issued the certicate is  generated using this method
     * S-16480 T-23119 Automation to generating the Certificate of Occupan
     */
     public Static string createFileResponse(String title, Blob pageData, Set<id> attachmentRecId){
         try{
             List<ContentDocumentLink> codList =  new  List<ContentDocumentLink>();
             List<Task> tsList =  new List<Task>();
             //Insert ContentVersion
             String FileExtension='pdf';
             ContentVersion cVersion = new ContentVersion();
             cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
             cVersion.PathOnClient = title + '.' + FileExtension;//File name with extention
             
             cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
             cVersion.Title = title;//Name of the file
             cVersion.VersionData = pageData;//File content
 
 
             Insert cVersion;
             
             //After saved the Content Verison, get the ContentDocumentId
             Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
             Integer count= 0;
             for(Id recId : attachmentRecId){
             //Insert ContentDocumentLink
             ContentDocumentLink cDocLink = new ContentDocumentLink();
             cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
             cDocLink.LinkedEntityId = recId;//Add attachment parentId
             cDocLink.ShareType = 'V';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
                codList.add(cDocLink);
             // if(ContentDocumentLink.sObjectType.getDescribe().isCreateable()){
                 
             // }
                Task ts = new Task();
                ts.Subject = 'Email: '+title;
                ts.TaskSubtype = 'Email';
                ts.ActivityDate = Date.today();
                ts.Description =  '';
                ts.WhatID      =  recId;
               
                ts.Status = 'Completed';
                if(count!=0){
                tsList.add(ts);
                }
                count++;
             }
             if(!tsList.isEmpty()){
                insert tsList;
            }
             if(!codList.isEmpty()){
                 insert codList;
             }
             return cVersion.Id;
             
         }catch(Exception e){
             throw new AuraHandledException(e.getMessage() + e.getStackTraceString());
         } 
         
     }

}